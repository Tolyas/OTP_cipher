package com.tolyasdev;

import javax.swing.*;

/**
 * Created by tolyas on 6/22/16.
 */
public class MainForm extends JFrame {
    private JTextArea taPlainText;
    private JButton btnEncrypt;
    private JPanel rootPanel;
    private JButton btnDecrypt;
    private ActionListener actionListener;

    MainForm(ActionListener actionListener){
        super("OTP");
        this.actionListener = actionListener;
        setContentPane(rootPanel);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        taPlainText.setLineWrap(true);
        btnEncrypt.addActionListener(e -> actionListener.onEncryptClick(taPlainText.getText()));
        btnDecrypt.addActionListener(e -> actionListener.onDecryptClick(taPlainText.getText()));
        setVisible(true);
    }

    void setText(String txt){
        taPlainText.setText(txt);
    }

    interface ActionListener{
        void onEncryptClick(String plainText);
        void onDecryptClick(String plainText);
    }
}
