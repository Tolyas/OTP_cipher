package com.tolyasdev;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.Base64;

public class Main {
    private static String path = "";
    private static File[] listOfFiles;
    private static MainForm mainForm;
    public static void main(String[] args) {
        loadData();
        mainForm = new MainForm(actionListener);
    }

    private static void loadData(){
        try {
            path = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParent();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        File folder = new File(path + "/data");
        listOfFiles = folder.listFiles();

        System.out.println(path);

        if (listOfFiles != null) {
            for (int i = 0; i < listOfFiles.length - 1; i++) {
                for (int j = i + 1; j < listOfFiles.length; j++){
                    if(listOfFiles[i].getName().compareTo(listOfFiles[j].getName()) > 0){
                        File f = listOfFiles[i];
                        listOfFiles[i] = listOfFiles[j];
                        listOfFiles[j] = f;
                    }
                }
            }
        } else {
            JOptionPane.showMessageDialog(mainForm, "Ключи не найдены! Они должны быть в 'data/'.", "Ошибка!", JOptionPane.ERROR_MESSAGE);
        }
    }

    private static byte[] addFromFile(byte[] target, File file, int start){
        FileInputStream fileInputStream = null;
        byte[] bFile = new byte[(int) file.length()];
        try {
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
            fileInputStream.close();
            for (int i = 0; i < bFile.length; i++) {
                if(start + i < target.length) {
                    target[start + i] = bFile[i];
                } else {
                    break;
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return target;
    }

    private static void cutFile(File file, int start){
        FileInputStream fileInputStream = null;
        byte[] bFile = new byte[(int) file.length()];
        byte[] bNewFile = new byte[(int) file.length() - start];
        try {
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
            fileInputStream.close();
            for (int i = 0; i < bFile.length; i++) {
                if(i > start) {
                    bNewFile[i - start] = bFile[i];
                }
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(bNewFile);
            fileOutputStream.close();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private static byte[] otp(byte[] plain, byte[] cipher){
        byte[] result = new byte[plain.length];
        for (int i = 0; i < plain.length; i++){
            result[i] = (byte) (plain[i] ^ cipher[i]);
        }
        return result;
    }

    private static MainForm.ActionListener actionListener = new MainForm.ActionListener() {
        @Override
        public void onEncryptClick(String plainText) {
            byte[] plain = plainText.getBytes();
            byte[] key = loadKey(plain.length);
            mainForm.setText(Base64.getEncoder().encodeToString(otp(plain, key)));
            removeKey(plain.length);
        }

        @Override
        public void onDecryptClick(String plainText) {
            byte[] plain = Base64.getDecoder().decode(plainText);
            byte[] key = loadKey(plain.length);
            try {
                mainForm.setText(new String(otp(plain, key), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            removeKey(plain.length);
        }
    };

    private static byte[] loadKey(int plainLength){
        byte[] key = new byte[plainLength];
        int startPosition = 0;
        for (File listOfFile : listOfFiles) {
            if (listOfFile.length() < plainLength) {
                plainLength -= listOfFile.length();
                key = addFromFile(key, listOfFile, startPosition);
                startPosition += listOfFile.length();
            } else {
                key = addFromFile(key, listOfFile, startPosition);
                break;
            }
        }
        return key;
    }

    private static void removeKey(int plainLength){
        for (File listOfFile : listOfFiles) {
            if (listOfFile.length() < plainLength) {
                listOfFile.delete();
                plainLength -= listOfFile.length();
            } else {
                cutFile(listOfFile, plainLength);
                plainLength = 0;
            }
        }
        loadData();
    }
}
